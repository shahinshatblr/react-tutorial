import React, { useState, useEffect } from 'react';
import './App.css';
import List from "./component/list"
import InputComponent from "./component/inputComponent"
import { Outlet, Link, useNavigate } from "react-router-dom";

function App() {

  const [todoList, setTodoList] = useState(["todo 1", "todo 2", "todo 3"])
  const [todoItem, setTodoItem] = useState("")
  let navigate = useNavigate()


  const setInput = (value) => {
    setTodoItem(value)
  }

  const addNewTodo = () => {
    setTodoList([...todoList, todoItem])
    setTodoItem("")
  }
  const deleteTodo = (index) => {
    let temp = [...todoList]
    temp.splice(index, 1)
    setTodoList(temp)
  }
  const redirectFunction = (arg) => {
    navigate(arg) 
  }


return (
  <div className="App">

    <List deleteTodoList={deleteTodo} listProps={todoList} />
    <List deleteTodoList={deleteTodo} listProps={["name1","name2" ,"name 3"]} />
    {/* <div className="inputContainer">

        <input value={todoItem} type="text" onChange={(e) => setInput(e.target.value)} />

        <button className="addBtn" onClick={(e) => addNewTodo()} >Add</button>
      </div> */}
    <InputComponent todoItem={todoItem} setInput={setInput} addNewTodo={addNewTodo} >
      
    </InputComponent >
    {/* <Outlet  /> */}
    <Link to="1" >page 1</Link>
    <button onClick={() => redirectFunction("/2")}>page 2</button>

    <button onClick={() => redirectFunction("/form")}>Form</button>
  </div>
);
}

export default App;
