import React from 'react';
import ReactDOM from 'react-dom/client';
import {
  BrowserRouter,
  Routes,
  Route,
  Navigate,
  Outlet
} from "react-router-dom";
import './index.css';
import App from './App';
import NewTodo from "./pages/newTodo"
import NewTodo2 from "./pages/newTodo2"

import Form from "./pages/form"

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <div>
    {/* header will load in all routes */}
    <h2>Header</h2>
    <BrowserRouter>
      <Routes>
        {/* ======== add replace to actually redirect to app component ======== */}
        <Route path="/" element={<Navigate replace to="/2" />} />
        <Route path="/app" element={<div className="outer"><Outlet /></div>} >

          <Route path="1" element={<NewTodo />} />
          {/* <Route path="2" element={<NewTodo2 />} /> */}
          <Route index element={<App />} />
        </Route>
        {/* <Route path="/app" element={<App />} >
        <Route path="1" element={<NewTodo />} />
        <Route path="2" element={<NewTodo2 />} />
        </Route> */}
        <Route path="/2" element={<NewTodo2 />} />
        <Route path="/form" element={< Form />} />
      </Routes>
    </BrowserRouter>
  </div>
);

