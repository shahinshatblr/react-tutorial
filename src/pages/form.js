import React, { useState, useReducer } from 'react';


function Form() {

    const formSubmitFn = (e) => {
        e.preventDefault()
        if (!form.name) {
            // alert("name required")
            setForm({ nameError: "name required" })
        } else if (form.name.length <8 ) {
            setForm({ nameError: "min 8 required" })
        } else if (!form.password) {
            alert("password required")
        }
        else {
            alert("success")

        }
    }
    const updateForm = (prevForm, newForm) => { return { ...prevForm, ...newForm } }
    const [form, setForm] = useReducer(updateForm, { name: "", password: "", nameError: "" })
    console.log(form, "form ======")
    return (
        <div>
            <div>
                form
</div>
            <form onSubmit={(e) => formSubmitFn(e)}>
                <div>

                    <input placeholder="name" onChange={(e) => setForm({ name: e.target.value })} type="text" />
                    {form.nameError ? <spam>{form.nameError}</spam> : null}
                </div>
                <div>

                    <input placeholder="password" onChange={(e) => setForm({ password: e.target.value })} type="password" />
                </div>
                <button type="submit" >submit</button>
            </form>
        </div>
    );
}

export default Form;
