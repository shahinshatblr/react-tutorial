import React, { useState, useEffect } from 'react';


function NewTodo2() {
  const [loader, updateLoader] = useState(true)
  const [count, updateCount] = useState(0)
  const [countNew, updateCountNew] = useState(0)
  useEffect(() => {
    setTimeout(() => { updateLoader(false) }, 2000)
  }, [])
  useEffect(() => {
    if(count || countNew)
    alert(`count1 ${ count} count two${ countNew}`)
  }, [count ,countNew])
  console.log(countNew, "count")

  return (
    <div>
      { loader ?
        <div className="App">loading ........</div> :
        <div className="App">
          page 2
          <button onClick={() => updateCount((prev) => prev + 1)}>update count</button>
          <button onClick={() => updateCountNew((prev) => prev + 1)}>update count New</button>
        </div>}
    </div>
  );
}

export default NewTodo2;