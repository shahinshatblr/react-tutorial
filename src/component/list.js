import React, { useState } from 'react';
import '../App.css';

function List(props) {
    return (
        <ul>
            {props.listProps.map((item, index) =>
                <li>
                    {item} <span onClick={()=>props.deleteTodoList(index)}  className="close">x</span>
                </li>)
            }
        </ul>

    );
}

export default List;
