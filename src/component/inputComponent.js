import React, { useState } from 'react';
import '../App.css';

function InputComponent(props) {
    const newFunction = ()=>{
        console.log("aa")
    }
    return (
        <div className="inputContainer">

        <input value = {props.todoItem} onChange={(e)=>props.setInput(e.target.value)}type="text"  />

        <button className="addBtn" onClick={()=>props.addNewTodo()}  >Add</button>
      </div>

    );
}

export default InputComponent;
